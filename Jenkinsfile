pipeline {

    agent {
        label 'docker-ce'
    }

    stages {
        stage('Build and publish') {
            agent {
                docker {
                    image 'europeanspallationsource/oracle-jdk-maven-jenkins:8u161-b12-1'
                    reuseNode true
                }
            }
            steps {
                script {
                    env.POM_VERSION = readMavenPom().version
                    currentBuild.displayName = env.POM_VERSION
                }
                withCredentials([usernamePassword(credentialsId: 'artifactory', usernameVariable: 'USERNAME', passwordVariable: 'PASSWORD')]) {
                    sh 'mvn --batch-mode -Dartifactory.username=${USERNAME} -Dartifactory.password=${PASSWORD} clean deploy'
                }
            }
        }
        stage('SonarQube analysis') {
            agent {
                docker {
                    image 'europeanspallationsource/oracle-jdk-maven-jenkins:8u161-b12-1'
                    reuseNode true
                }
            }
            steps {
                withCredentials([string(credentialsId: 'sonarqube', variable: 'TOKEN')]) {
                    sh 'mvn --batch-mode -Dsonar.scm.disabled=true -Dsonar.login=$TOKEN -Dsonar.branch=${BRANCH_NAME} sonar:sonar'
                }
            }
        }
    }

    post {
        failure {
            slackSend (color: 'danger', message: "FAILED: <${env.BUILD_URL}|${env.JOB_NAME} [${env.BUILD_NUMBER}]>")
        }
        success {
            slackSend (color: 'good', message: "SUCCESSFUL: <${env.BUILD_URL}|${env.JOB_NAME} [${env.BUILD_NUMBER}]>")
        }
    }
}
