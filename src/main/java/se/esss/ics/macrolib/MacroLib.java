/*
 * Copyright (c) 2015 European Spallation Source
 * Copyright (c) 2015 Cosylab d.d.
 *
 * This file is part of Controls Configuration Database.
 *
 * Controls Configuration Database is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the License,
 * or any newer version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package se.esss.ics.macrolib;

import com.google.common.collect.ImmutableSet;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.LineNumberReader;
import java.nio.charset.StandardCharsets;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * Class that provides both macro parsing from text streams and macro expansion functionality
 * for EPICS iocsh kind of macros.
 *
 * Supported macro formats are ${MACRO} or $(MACRO).
 * Default macro functionality is also supported, e.g. $(MACRO1=blah${MACRO2})
 *
 * @author <a href="mailto:miroslav.pavleski@cosylab.com">Miroslav Pavleski</a>
 */
public class MacroLib {
    private static final String IO_ERR_MSG = "IO error while reading input";

    private MacroStorage macroStorage;

    private int currentLineNo = 0;

    private static class ExpansionCtx {
        private final Function<String,String> resolver;
        private final StringBuilder output = new StringBuilder();

        ExpansionCtx(Function<String, String> resolver) {
            this.resolver = resolver;
        }

        Function<String, String> getResolver() { return resolver; }
        StringBuilder getOutput() { return output; }
    }

    private ExpansionCtx expCtx = null;

    private static enum State {
        OUTSIDE,
        MACRO_NAME
    }

    /** Parses the macros in a {@link String}
     * To retrieve the macros please use the get methods on this class.
     * @param str input text with macros
     * @throws MacroException
     */
    public void parseMacros(String str) throws MacroException {
        parseMacros(new ByteArrayInputStream(str.getBytes(StandardCharsets.US_ASCII)));
    }

    /** Parses the macros in a text {@link File}
     * To retrieve the macros please use the get methods on this class.
     * @param file input file
     * @throws MacroException
     */
    public void parseMacros(File file) throws MacroException {
        try (FileInputStream fis = new FileInputStream(file)) {
            parseMacros(fis);
        } catch (IOException ex) {
            throw new MacroException(IO_ERR_MSG, ex);
        }
    }

    /** Parses the macros in a {@link InputStream}
     * To retrieve the macros please use the get methods on this class.
     * @param is the input stream
     * @throws MacroException
     */
    public void parseMacros(InputStream is) throws MacroException {
        resetState();

        try (LineNumberReader reader = new LineNumberReader(new InputStreamReader(is, StandardCharsets.US_ASCII))) {
            String line;
            while ((line = reader.readLine()) != null) {
                currentLineNo = reader.getLineNumber();
                findMacros(line);
            }
        } catch (IOException ex) {
            throw new MacroException(IO_ERR_MSG, ex);
        }
    }

    /** Expands the macros in the input using definitions in the resolver argument
     * The definitions can contain other macros.
     * @param line the input line containing macros
     * @param resolver a String,String mapping functions that provides definitions for macros
     * @throws MacroException
     * @return
     */
    public String expandMacros(String line, Function<String, String> resolver) throws MacroException {
        resetState();

        expCtx = new ExpansionCtx(resolver);
        findMacros(new HashSet<>(), line, 0, '\00', new HashSet<>(), true);
        return expCtx.getOutput().toString();
    }

    /**
     * Expands the macros in the input using definitions in the definitions map argument
     * @param line the input line containing macros
     * @param definitions a String, String {@link Map} that provides definitions for the macros
     * @return
     * @throws MacroException
     */
    public String expandMacros(String line, Map<String, String> definitions) throws  MacroException
    {
        final Map<String, String> upperCaseMap = definitions.entrySet().stream().
                collect(Collectors.toMap(entry -> entry.getKey().toUpperCase(), entry -> entry.getValue()));
        return expandMacros(line, name -> upperCaseMap.get(name.toUpperCase()));
    }

    /** Getter for the parsed macros storage
     * @return the parsed macros
     */
    public MacroStorage getMacroStorage() { return macroStorage; }

    private void resetState() {
        macroStorage = new MacroStorage();
        expCtx = null;
    }

    private void findMacros(String line) throws MacroException {
        findMacros(new HashSet<>(), line, 0, '\00', new HashSet<>(), false);
    }

    private void handleExpansionChar(char ch) {
        if (expCtx != null) {
            expCtx.getOutput().append(ch);
        }
    }

    private void handleExpansionMacroName(String macroName, Set<String> outer) throws MacroException {
        if (expCtx != null) {
            final String def = expCtx.getResolver().apply(macroName.toUpperCase());
            if (def == null) {
                throw new MacroException(String.format("No definition for macro %s", macroName));
            } else {
                findMacros(new HashSet<>(), def, 0, '\00', outer, true);
            }
        }
    }

    private static <T> ImmutableSet<T> addToImmutableSet(Set<T> set, T entry) {
        final ImmutableSet.Builder<T> setBuilder = ImmutableSet.builder();
        setBuilder.addAll(set);
        setBuilder.add(entry);
        return setBuilder.build();
    }

    private int findMacros(Set<MacroEntry> outerDep,
            String line, int pos, char terminator,
            Set<String> outerMacros, boolean writeToOutput) throws MacroException {
        State state = State.OUTSIDE;

        char macroEnd = 0;
        StringBuilder macroName = new StringBuilder();

        int macroStartColumn = 0;

        int i=pos;
        for (; i < line.length(); i++) {
            char ch = line.charAt(i);
            char nextCh = i+1 < line.length() ? line.charAt(i+1) : 0;

            // Additional loop exit condition
            if (ch != macroEnd && ch == terminator) {
                break;
            }

            switch (state) {
                case OUTSIDE:
                    if (ch=='$' && (nextCh == '(' || nextCh == '{')) {
                        macroStartColumn = i+1;

                        i++; // will skip the current $ and the next { or (
                        macroEnd = nextCh == '{' ? '}' : ')';
                        state = State.MACRO_NAME;
                        macroName = new StringBuilder();
                    } else if (writeToOutput) {
                        handleExpansionChar(ch);
                    }
                    break;
                case MACRO_NAME:
                    if (ch == macroEnd) {
                        final String macroNameStr = macroName.toString();
                        macroFound(macroNameStr, null, outerDep, macroStartColumn, i+1);

                        if (outerMacros.contains(macroNameStr)) {
                            throw new MacroException(String.format("Recursive macro expansion for macro %s !",
                                    macroNameStr));
                        } else if (writeToOutput) {
                            handleExpansionMacroName(macroNameStr, addToImmutableSet(outerMacros, macroNameStr));
                        }

                        macroEnd = 0;
                        state = State.OUTSIDE;
                    } else if (ch == '=') {
                        if (nextCh != 0) {
                            final String macroNameStr = macroName.toString();

                            MacroDefaultValue defValue = new MacroDefaultValue();

                            int defValueStartColumn = i+1;

                            int advancedPos;
                            if (expCtx == null) {
                                advancedPos = findMacros(defValue.getDependentMacros(),
                                        line, i+1, macroEnd,
                                        addToImmutableSet(outerMacros, macroNameStr), false);
                            } else {
                                final String def = expCtx.getResolver().apply(macroNameStr.toUpperCase());

                                if (def != null) {
                                    // do not write ouput, just advance
                                    advancedPos = findMacros(defValue.getDependentMacros(),
                                        line, i+1, macroEnd, addToImmutableSet(outerMacros, macroNameStr), false);

                                    // Write to output the expansion of the def value found
                                    findMacros(new HashSet<>(), def, 0, '\00', outerMacros, true);
                                } else {
                                    // Expand the default value
                                    advancedPos = findMacros(defValue.getDependentMacros(),
                                        line, i+1, macroEnd, addToImmutableSet(outerMacros, macroNameStr), true);
                                }
                            }

                            defValue.setRawValue(line.substring(i+1, advancedPos));
                            defValue.setFilePosition(new FilePosition(currentLineNo, defValueStartColumn));

                            state = State.OUTSIDE;
                            i = advancedPos;
                            macroFound(macroNameStr, defValue, outerDep, macroStartColumn, i+1);
                            macroEnd = 0;
                        }
                    } else {
                        macroName.append(ch);
                    }
                    break;
            }

        }

        if (state != State.OUTSIDE) {
            throw new MacroException("Open macro at line end.");
        }
        return i;
    }

    private void macroFound(final String macroName, MacroDefaultValue defValue,
            Set<MacroEntry> outerDep, int macroStartColumn, int endColumn) {
        final String upperCaseMacroName = macroName.toUpperCase();

        MacroEntry toUse = macroStorage.getMacros().get(upperCaseMacroName);
        if (toUse == null) {
            toUse = new MacroEntry(macroName);

            macroStorage.getOrderedMacros().add(toUse);
            macroStorage.getMacros().put(upperCaseMacroName, toUse);
        }

        final MacroOccurrence occurence = new MacroOccurrence(toUse, new FilePosition(currentLineNo,
                macroStartColumn, endColumn), defValue);
        toUse.getOccurrences().add(occurence);
        macroStorage.getOrderedMacroOccurrences().add(occurence);

        outerDep.add(toUse);
    }
}
