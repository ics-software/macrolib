/*
 * Copyright (c) 2015 European Spallation Source
 * Copyright (c) 2015 Cosylab d.d.
 *
 * This file is part of Controls Configuration Database.
 *
 * Controls Configuration Database is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the License,
 * or any newer version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package se.esss.ics.macrolib;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

/**
 *
 * @author <a href="mailto:miroslav.pavleski@cosylab.com">Miroslav Pavleski</a>
 */
public class MacroDefaultValue implements Serializable {
    private String rawValue;

    private FilePosition macroPosition;
    private FilePosition filePosition;

    private final Set<MacroEntry> dependentMacros = new HashSet<>();

    public MacroDefaultValue() {}
    public MacroDefaultValue(String rawValue) { this.rawValue = rawValue; }

    public String getRawValue() { return rawValue; }
    public void setRawValue(String rawValue) { this.rawValue = rawValue; }

    public FilePosition getMacroPosition() { return macroPosition; }
    public void setMacroPosition(FilePosition macroPosition) { this.macroPosition = macroPosition; }

    public FilePosition getFilePosition() { return filePosition; }
    public void setFilePosition(FilePosition filePosition) { this.filePosition = filePosition; }

    public Set<MacroEntry> getDependentMacros() { return dependentMacros; }
}
