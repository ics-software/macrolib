/*
 * Copyright (c) 2015 European Spallation Source
 * Copyright (c) 2015 Cosylab d.d.
 *
 * This file is part of Controls Configuration Database.
 *
 * Controls Configuration Database is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the License,
 * or any newer version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package se.esss.ics.macrolib;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Storage of parsed macros in various forms (ordered, ordered occurrences, string map
 *
 * @author <a href="mailto:miroslav.pavleski@cosylab.com">Miroslav Pavleski</a>
 */
public class MacroStorage implements Serializable {
    private final List<MacroEntry> orderedMacros = new ArrayList<>();
    private final List<MacroOccurrence> orderedMacroOccurrences = new ArrayList<>();
    private final Map<String, MacroEntry> macros = new HashMap<>();

    public List<MacroEntry> getOrderedMacros() { return orderedMacros; }
    public List<MacroOccurrence> getOrderedMacroOccurrences() { return orderedMacroOccurrences; }
    public Map<String, MacroEntry> getMacros() { return macros; }
}
