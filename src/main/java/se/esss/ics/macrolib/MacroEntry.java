/*
 * Copyright (c) 2015 European Spallation Source
 * Copyright (c) 2015 Cosylab d.d.
 *
 * This file is part of Controls Configuration Database.
 *
 * Controls Configuration Database is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the License,
 * or any newer version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package se.esss.ics.macrolib;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import org.apache.commons.lang3.StringUtils;

/**
 * Has information about macro and all occurrences of it in a text file
 *
 * @author <a href="mailto:miroslav.pavleski@cosylab.com">Miroslav Pavleski</a>
 */
public class MacroEntry implements Serializable {
    /** Macro name */
    private final String name;

    /** Represents the list of occurrences of the macro in a file */
    private final List<MacroOccurrence> occurrences = new ArrayList<>();

    public MacroEntry(String name) { this.name = name; }

    public String getName() { return name; }

    public List<MacroOccurrence> getOccurrences() { return occurrences; }

    @Override
    public int hashCode() { return Objects.hashCode(name != null ? name.toUpperCase() : ""); }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final MacroEntry other = (MacroEntry) obj;
        return StringUtils.equalsIgnoreCase(this.name, other.name);
    }
}
