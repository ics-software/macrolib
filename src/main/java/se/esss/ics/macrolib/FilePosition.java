/*
 * Copyright (c) 2015 European Spallation Source
 * Copyright (c) 2015 Cosylab d.d.
 *
 * This file is part of Controls Configuration Database.
 *
 * Controls Configuration Database is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the License,
 * or any newer version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package se.esss.ics.macrolib;

import java.io.Serializable;

/**
 * Represents the position of a sub-string in a line in a text-file
 *
 * @author <a href="mailto:miroslav.pavleski@cosylab.com">Miroslav Pavleski</a>
 */
public class FilePosition implements Serializable {
    private int line;
    private int column;
    private int endColumn;

    public FilePosition() { }
    public FilePosition(int line, int column) { this.line = line; this.column = column; }
    public FilePosition(int line, int column, int endColumn) {
        this.line = line;
        this.column = column;
        this.endColumn = endColumn;
    }

    public int getLine() { return line; }
    public void setLine(int line) { this.line = line; }

    public int getColumn() { return column; }
    public void setColumn(int column) { this.column = column; }

    public int getEndColumn() { return endColumn; }
    public void setEndColumn(int endColumn) { this.endColumn = endColumn; }
}
