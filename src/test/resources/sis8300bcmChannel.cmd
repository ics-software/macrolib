# @field ASYN_PORT
# @type STRING
# Name of the Asyn port
#
# @field DEVICE_FILE
# @type STRING 
# Device filename
#
# @field BUFFER_SIZE
# @type STRING 
# Data acq. buffer size
#
# @field FLOAT_FIELD
# @type FLOAT
# Name of float field
#
# @field DOUBLE_FIELD
# @type DOUBLE
# Name of the Double field
#
# @field INT_FIELD
# @type INTEGER
# name of the int field
#
# @field LINK_FIELD
# @type LINK
# Name of the link field
#



dbLoadRecords("sis8300AIChannel.template",  "PREFIX=$(PREFIX),CHANNEL_ID=$(CHANNEL_ID=AI$(ASYN_ADDR)),ASYN_ADDR=$(ASYN_ADDR),DECO_VAL=$(DECO_VAL=0),ASYN_PORT=$(ASYN_PORT),AI_FLNK=$(AI_FLNK=),DECF_VAL=$(DECF_VAL=100),LCVO_VAL=$(LCVO_VAL=-36.99),ATT_DRVL=$(ATT_DRVL=0),AI_NELM=$(AI_NELM),LCVF_VAL=$(LCVF_VAL=0.001129),IN_PREC=$(IN_PREC=3),ENBL_VAL=$(ENBL_VAL=1),AI_TSEL=$(AI_TSEL=),ATT_DRVH=$(ATT_DRVH=0)")
dbLoadRecords("sis8300bcmAIChannel.template", "PREFIX=$(PREFIX),CHANNEL_ID=$(CHANNEL_ID=AI${ASYN_ADDR}),ASYN_PORT=$(ASYN_PORT),ASYN_ADDR=$(ASYN_ADDR)")

