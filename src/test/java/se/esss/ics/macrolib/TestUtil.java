/*
 * Copyright (c) 2015 European Spallation Source
 * Copyright (c) 2015 Cosylab d.d.
 *
 * This file is part of Controls Configuration Database.
 *
 * Controls Configuration Database is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the License,
 * or any newer version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package se.esss.ics.macrolib;

import java.io.PrintStream;
import java.util.stream.Collectors;

/**
 *
 * @author <a href="mailto:miroslav.pavleski@cosylab.com">Miroslav Pavleski</a>
 */
public class TestUtil {
    public static void printAllMacros(MacroStorage macros, PrintStream out) {
        out.printf("\n\n-= ALL MACROS =-\n");
        macros.getOrderedMacros().stream().forEach( v -> {
            out.printf("%s  Occurences: %s\n", v.getName(),
                    v.getOccurrences().stream().
                            map(oc -> String.format("\n\t(%d, %d - %d) %s", oc.getPosition().getLine(),
                                    oc.getPosition().getColumn(),
                                    oc.getPosition().getEndColumn(),
                                    formatDefValue(oc.getDefValue()))).collect(Collectors.joining("\n")));
            out.println("");
        });
    }

    public static void printAllMacroOccurences(MacroStorage macros, PrintStream out) {
        out.printf("\n\n-= ALL OCCURENCES =-\n");
        macros.getOrderedMacroOccurrences().forEach( oc -> {
            out.print(String.format("%s:\t(%d, %d - %d) %s\n", oc.getMacro().getName(), oc.getPosition().getLine(),
                                    oc.getPosition().getColumn(),
                                    oc.getPosition().getEndColumn(),
                                    formatDefValue(oc.getDefValue())));
        });
    }

    private static String formatDefValue(MacroDefaultValue defValue) {
        if (defValue == null) {
            return "";
        }
        return String.format(" Default Value: %s    Dependent Macros: %s", defValue.getRawValue(),
                defValue.getDependentMacros().stream().map(dm -> dm.getName()).collect(Collectors.joining(", ")));
    }

}
