/*
 * Copyright (c) 2015 European Spallation Source
 * Copyright (c) 2015 Cosylab d.d.
 *
 * This file is part of Controls Configuration Database.
 *
 * Controls Configuration Database is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the License,
 * or any newer version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package se.esss.ics.macrolib;

import com.google.common.collect.ImmutableMap;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import org.junit.Test;

/**
 *
 * @author <a href="mailto:miroslav.pavleski@cosylab.com">Miroslav Pavleski</a>
 */
public class MacroLibTest {
    @Test
    public void testParsing() throws MacroException {
        final MacroLib mp = new MacroLib();
        mp.parseMacros( this.getClass().getResourceAsStream("/sis8300bcmChannel.cmd") );
        assertNotNull(mp.getMacroStorage().getOrderedMacros());
        assertEquals(15, mp.getMacroStorage().getOrderedMacros().size());
        TestUtil.printAllMacros(mp.getMacroStorage(), System.out);
        TestUtil.printAllMacroOccurences(mp.getMacroStorage(), System.out);
    }

    @Test(expected = MacroException.class)
    public void testParsingOpenMacro() throws MacroException {
        final MacroLib mp = new MacroLib();
        final String input = "PREFIX=$(PREFIX";
        mp.parseMacros(input);
    }

    @Test
    public void testValidExpansion() throws MacroException {
        final MacroLib mp = new MacroLib();
        final String input = "PREFIX=$(PREFIX=abc${ELSE=AA$(ELSE1)BB}def)";
        final ImmutableMap<String, String> definitions = ImmutableMap.of("ELSE1", "VALUE");
        final String output = mp.expandMacros(input, definitions);
        assertEquals("PREFIX=abcAAVALUEBBdef", output);
    }

    @Test
    public void testValidExpansionMixedCase() throws MacroException {
        final MacroLib mp = new MacroLib();
        final String input = "PREFIX=$(PREFIX=abc${ELSE=AA$(else1)BB}def)";
        final ImmutableMap<String, String> definitions = ImmutableMap.of("ELSe1", "VALUE");
        final String output = mp.expandMacros(input, definitions);
        assertEquals("PREFIX=abcAAVALUEBBdef", output);
    }

    @Test(expected = MacroException.class)
    public void testInvalidExpansionNoDefinition() throws MacroException {
        final MacroLib mp = new MacroLib();
        final String input = "PREFIX=$(PREFIX=abc${ELSE=AA$(NOVAL)BB}def)";
        final ImmutableMap<String, String> definitions = ImmutableMap.of("ELSE1", "VALUE");
        final String output =  mp.expandMacros(input, definitions);
    }

    @Test(expected = MacroException.class)
    public void testInvalidExpansionRecursion() throws MacroException {
        final MacroLib mp = new MacroLib();
        final String input = "PREFIX=$(PREFIX=abc${ELSE=AA$(ELSE)BB}def)";
        final ImmutableMap<String, String> definitions = ImmutableMap.of("ELSE", "VALUE");
        final String output = mp.expandMacros(input, definitions);
    }
}
